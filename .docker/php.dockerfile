FROM php:fpm-alpine

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN install-php-extensions intl pdo pdo_mysql mysqli json xdebug opcache apcu imagick zip pcntl exif

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN apk add nodejs

WORKDIR /var/www/html